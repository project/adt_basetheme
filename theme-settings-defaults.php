<?php

  $defaults = array(
    'fixedfluid'                        => 'px',
    'layout_width_fluid'                => 80,
    'layout_width_fixed'                => 820,
    'sidebar_width'                     => '20',
    'expand_admin'                      => 0,
    'layout_min_width'                  => 680,
    'layout_max_width'                  => 1300,
    'headings_font_face'                => 'arial',
    'body_font_face'                    => 'arial',
    'cufon_enable'                      => 1,
    'cufon_font_face'                   => 'Gentium_Basic_Bold.js',
    'taxonomy_display_default'          => 'only',
    'taxonomy_format_default'           => 'vocab',
    'taxonomy_enable_content_type'      => 0,
    'readmore_default'                  => t('Read more'),
    'readmore_title_default'            => t('Read the rest of this article.'),
    'readmore_prefix_default'           => '',
    'readmore_suffix_default'           => '',
    'readmore_enable_content_type'      => 0,
    'comment_singular_default'          => t('1 comment'),
    'comment_plural_default'            => t('@count comments'),
    'comment_title_default'             => t('Go to the first comment.'),
    'comment_prefix_default'            => '',
    'comment_suffix_default'            => '',
    'comment_new_singular_default'      => t('1 new comment'),
    'comment_new_plural_default'        => t('@count new comments'),
    'comment_new_title_default'         => t('Go to the first new comment.'),
    'comment_new_prefix_default'        => '',
    'comment_new_suffix_default'        => '',
    'comment_add_default'               => t('Add new comment'),
    'comment_add_title_default'         => t('Add a new comment to this page.'),
    'comment_add_prefix_default'        => '',
    'comment_add_suffix_default'        => '',
    'comment_enable_content_type'       => 0,
    'commentheader'                     => 'Comments:',
    'superfish_enable'                  => 1,
    'superfish_speed'                   => 500,
    'superfish_delay'                   => 500,
    'superfish_easing'                  => 'swing',
    'superfish_properties'              => array('opacity' => TRUE, 'height' => TRUE),
    'iepngfix'                          => 1,
    'link_icons'                        => 0,
    'superfish'                         => 1,
    'block_edit'                        => 1,
    'force_eq_heights'                  => 1,
    'input_example'                     => 1,
    'timestamp'                         => 0,
  );

  // Make the default content-type settings the same as the default theme settings,
  // so we can tell if content-type-specific settings have been altered.
  $defaults = array_merge($defaults, theme_get_settings());

  // Get the node types
  $node_types = node_get_types('names');

  // Set the default values for content-type-specific settings
  foreach ($node_types as $type => $name) {
    $defaults["taxonomy_display_{$type}"]         = $defaults['taxonomy_display_default'];
    $defaults["taxonomy_format_{$type}"]          = $defaults['taxonomy_format_default'];
    $defaults["submitted_by_author_{$type}"]      = $defaults['submitted_by_author_default'];
    $defaults["submitted_by_date_{$type}"]        = $defaults['submitted_by_date_default'];
    $defaults["readmore_{$type}"]                 = $defaults['readmore_default'];
    $defaults["readmore_title_{$type}"]           = $defaults['readmore_title_default'];
    $defaults["readmore_prefix_{$type}"]          = $defaults['readmore_prefix_default'];
    $defaults["readmore_suffix_{$type}"]          = $defaults['readmore_suffix_default'];
    $defaults["comment_singular_{$type}"]         = $defaults['comment_singular_default'];
    $defaults["comment_plural_{$type}"]           = $defaults['comment_plural_default'];
    $defaults["comment_title_{$type}"]            = $defaults['comment_title_default'];
    $defaults["comment_prefix_{$type}"]           = $defaults['comment_prefix_default'];
    $defaults["comment_suffix_{$type}"]           = $defaults['comment_suffix_default'];
    $defaults["comment_new_singular_{$type}"]     = $defaults['comment_new_singular_default'];
    $defaults["comment_new_plural_{$type}"]       = $defaults['comment_new_plural_default'];
    $defaults["comment_new_title_{$type}"]        = $defaults['comment_new_title_default'];
    $defaults["comment_new_prefix_{$type}"]       = $defaults['comment_new_prefix_default'];
    $defaults["comment_new_suffix_{$type}"]       = $defaults['comment_new_suffix_default'];
    $defaults["comment_add_{$type}"]              = $defaults['comment_add_default'];
    $defaults["comment_add_title_{$type}"]        = $defaults['comment_add_title_default'];
    $defaults["comment_add_prefix_{$type}"]       = $defaults['comment_add_prefix_default'];
    $defaults["comment_add_suffix_{$type}"]       = $defaults['comment_add_suffix_default'];
  }

  // Merge the saved variables and their default values
  $settings = array_merge($defaults, $saved_settings);

  // If content type-specifc settings are not enabled, reset the values
  if (!$settings['readmore_enable_content_type']) {
    foreach ($node_types as $type => $name) {
      $settings["readmore_{$type}"]                    = $settings['readmore_default'];
      $settings["readmore_title_{$type}"]              = $settings['readmore_title_default'];
      $settings["readmore_prefix_{$type}"]             = $settings['readmore_prefix_default'];
      $settings["readmore_suffix_{$type}"]             = $settings['readmore_suffix_default'];
    }
  }
  if (!$settings['comment_enable_content_type']) {
    foreach ($node_types as $type => $name) {
      $defaults["comment_singular_{$type}"]         = $defaults['comment_singular_default'];
      $defaults["comment_plural_{$type}"]           = $defaults['comment_plural_default'];
      $defaults["comment_title_{$type}"]            = $defaults['comment_title_default'];
      $defaults["comment_prefix_{$type}"]           = $defaults['comment_prefix_default'];
      $defaults["comment_suffix_{$type}"]           = $defaults['comment_suffix_default'];
      $defaults["comment_new_singular_{$type}"]     = $defaults['comment_new_singular_default'];
      $defaults["comment_new_plural_{$type}"]       = $defaults['comment_new_plural_default'];
      $defaults["comment_new_title_{$type}"]        = $defaults['comment_new_title_default'];
      $defaults["comment_new_prefix_{$type}"]       = $defaults['comment_new_prefix_default'];
      $defaults["comment_new_suffix_{$type}"]       = $defaults['comment_new_suffix_default'];
      $defaults["comment_add_{$type}"]              = $defaults['comment_add_default'];
      $defaults["comment_add_title_{$type}"]        = $defaults['comment_add_title_default'];
      $defaults["comment_add_prefix_{$type}"]       = $defaults['comment_add_prefix_default'];
      $defaults["comment_add_suffix_{$type}"]       = $defaults['comment_add_suffix_default'];
    }
  }