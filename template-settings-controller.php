<?php

// Initialize theme settings
if (is_null(theme_get_setting('fixedfluid'))) {  // <-- if settings have never ben set

  // get theme key
  global $theme_key;

  // include theme settings defaults
  if(is_file($theme_path . 'theme-settings-defaults.php')) {
    include('theme-settings-defaults.php');
  }

  // Don't save the toggle_node_info_ variables
  if (module_exists('node')) {
    foreach (node_get_types() as $type => $name) {
      unset($settings['toggle_node_info_'. $type]);
    }
  }
  // Save default theme settings
  variable_set(
    str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
    array_merge($defaults, $settings)
  );
  // Force refresh of Drupal internals
  theme_get_setting('', TRUE);
}

if (theme_get_setting('timestamp') > 0) {
  GLOBAL $theme;
  drupal_add_css(file_directory_path().'/cto-'.$theme.'.css', 'theme', 'all');
}

if (theme_get_setting('cufon_enable')) {
  //Load Cufon font replacement scripts
  if(is_file($theme_path .'scripts/typography/cufon-yui.js')) {
  drupal_add_js($theme_path .'scripts/typography/cufon-yui.js');
  }
  if(is_file($theme_path .'scripts/typography/fonts/'. theme_get_setting('cufon_font_face'))) {
  drupal_add_js($theme_path .'scripts/typography/fonts/'. theme_get_setting('cufon_font_face'));
  }
  if(is_file($theme_path .'scripts/typography/invoke-cufon.js')) {
  drupal_add_js($theme_path .'scripts/typography/invoke-cufon.js');
  }
}
if (theme_get_setting('superfish_enable')) {
  //Load superfish drop-down scripts
  if ((theme_get_setting('superfish_delay') > 0)) {
    if(is_file($theme_path .'scripts/drop_down/hoverIntent.js')) {
    drupal_add_js($theme_path .'scripts/drop_down/hoverIntent.js');
    }
  }
  if(is_file($theme_path .'scripts/drop_down/adt-superfish.js')) {
  drupal_add_js($theme_path .'scripts/drop_down/adt-superfish.js');
  }
  if(is_file($theme_path .'scripts/drop_down/invoke-superfish.js')) {
  drupal_add_js($theme_path .'scripts/drop_down/invoke-superfish.js');
  }
  foreach (theme_get_setting('superfish_properties') as $property => $value) {
    if ($value){
      drupal_add_js(array('superfish' => array('animation' => array($property => 'show'))), 'setting');
    }
  }
  drupal_add_js(array('superfish' => array('speed' => theme_get_setting('superfish_speed'))), 'setting');
  drupal_add_js(array('superfish' => array('delay' => theme_get_setting('superfish_delay'))), 'setting');
  drupal_add_js(array('superfish' => array('easing' => theme_get_setting('superfish_easing'))), 'setting');
}

// Load jQuery example plugin
if (theme_get_setting('input_example')) {
    drupal_add_js($theme_path .'scripts/misc/jquery.example.min.js');
}

// Load jQuery equal heights plugin
if (theme_get_setting('force_eq_heights')) {
  if(is_file($theme_path .'scripts/misc/jQuery.equalHeights.js')) {
    drupal_add_js($theme_path .'scripts/misc/jQuery.equalHeights.js');
  }
}

/**
CSS Caching Workflow:

* 1. A file is created in /files
* 2. Theme settings and derivative variables are used to make up CSS style rules
* 3. The file is filled with the generated CSS style rules.
* 4. On each pageload, the last-modified time of the file is compared to the last-modified time that is saved as a hidden form field in the themesettings form.
* 5.a If the microtimes are the same, theme settings are not updated
* 5.b. If the microtimes are different, theme settings have been re-saved (and possibly changed). Theme settings will be loaded from the database and derivative css styles will be generated.
* 6. The themesettings stylesheet will be filled with the newly generated CSS

**/
if (user_access('access administration pages') && (arg(2) == 'themes') && (arg(3) == 'settings') && (arg(4) != FALSE)) {
GLOBAL $theme;

if (theme_get_setting('timestamp')) { // If theme settings are in fact "customized", this only returns FALSE if the theme settings never have been saved
  $newTimestamp = "/*".theme_get_setting('timestamp')."*/\n";
  $ctoCssFile = file_directory_path().'/cto-'.$theme.'.css';
  if (is_file($ctoCssFile)) {
    $fh = fopen($ctoCssFile, 'r');
    $oldTimestamp = fgets($fh,1024); // extract timestamp from first line of css file. Note: the timestap is in a css file so it is wrapped in /* css comment */ characters.
    fclose($fh);
  }

  if (($oldTimestamp) != ($newTimestamp)) { // if custom theme options have been saved since last time css file was written
    /* Construct CSS file: */

    $CSS = '';
    $layout_width_fixed = theme_get_setting('layout_width_fixed');
    $layout_width_fluid = theme_get_setting('layout_width_fluid');
    $layout_min_width = theme_get_setting('layout_min_width');
    $layout_max_width = theme_get_setting('layout_max_width');

    if (theme_get_setting('fixedfluid') == 'px') {
      $CSS .= "#container { width: {$layout_width_fixed}px; }\n";
    } else {
      $CSS .= "#container { width: {$layout_width_fluid}%; }\n";
    }
    if ($layout_min_width) {
      $CSS .= "body #container { min-width: {$layout_min_width}px; }\n";
    } else {
    $CSS .= "body #container { min-width: none; }\n";
    }
    if ($layout_max_width) {
      $CSS .= "body #container { max-width: {$layout_max_width}px; }\n";
    } else {
      $CSS .= "body #container { max-width: none; }\n";
    }
    if (theme_get_setting('expand_admin')):
      $CSS .= "body.section-admin #container,body.section-admin #footer { width:90%; }\n";
    endif;
    
    if (theme_get_setting('body_font_face')):
      $font_set = font_stack(theme_get_setting('body_font_face'));
      $CSS .= "html body { font-family: {$font_set}; }\n";
    endif;
    
    if (theme_get_setting('headings_font_face')):
      $font_set = font_stack(theme_get_setting('headings_font_face'));
      $CSS .= "h1, h2, h3, h4, h5, h6, legend, p.mission, p.slogan { font-family: {$font_set}; }\n";
    endif;    

/**
* Color module injection
* Writes the colors to the stylesheet, using the color_get_palette('themename') function, and assisting color-calibration functions if applicable
* (color module's own color shifting in CSS works unreliably and inaccurately)
*/    
    if (module_exists(color)):
    // Override colors in style.css with color palette from theme settings. All selectors have increased specificity so that they will override other stylesheets and not be overriden
      $ctopalette = color_get_palette($theme);
      $CSS .= "html body, body .nodetitle a:hover, body .nodetitle a:focus { color:".$ctopalette['text'].";}\n";
      $CSS .= "html body a { color:".$ctopalette['top'].";}\n";
      $CSS .= "html body, .poll .bar .foreground { background-color:".$ctopalette['base'].";}\n";
      $CSS .= "body .adtblocks .block .block-inner { background-color:".$ctopalette['link'].";}\n";
      $CSS .= "body h1, body h2, body h3, body h4, body h5, body h6, legend, p.mission, p.slogan { color:".$ctopalette['bottom'].";}\n";
    endif;

    $fh = fopen($ctoCssFile, 'w');
    if ($fh) {
      fwrite($fh, $newTimestamp); // write new timestamp to file
      fwrite($fh, $CSS); // write css to file
    } else {
      drupal_set_message(t('Cannot write theme-settings file, please check your file system. (Visit status report page)'), 'error');
    }
    fclose($fh);
    // If the CSS & JS aggregation performance options are enabled we need to clear the caches
    drupal_clear_css_cache();
    drupal_clear_js_cache();
  }
} else {
$msgthemeset = 'Hello administrator! It looks like you\'ve never set your theme settings before, check out out the <a href="'. base_path() .'admin/build/themes/settings/'. $theme .'">theme settings page</a> to learn about the options.';
drupal_set_message($msgthemeset, 'message');
}
}

/**
 * Provides Font Stack values for theme settings which are written to custom.css
 * @see font_list()
 * @param $attributes
 * @return string
 */
function font_stack($font) {
  if ($font) {
    $fonts = array(
      'helvetica' => 'Arial, Helvetica, "Nimbus Sans L", "Liberation Sans", "FreeSans", sans-serif',
      'verdana' => 'Verdana, "Bitstream Vera Sans", Arial, sans-serif',
      'lucida' => '"Lucida Grande", "Lucida Sans", "Lucida Sans Unicode", "DejaVu Sans", Arial, sans-serif',
      'geneva' => '"Geneva", "Bitstream Vera Serif", "Tahoma", sans-serif',
      'tahoma' => 'Tahoma, Geneva, "DejaVu Sans Condensed", sans-serif',
      'century' => '"Century Gothic", "URW Gothic L", Helvetica, Arial, sans-serif',
      'georgia' => 'Georgia, "Bitstream Vera Serif", serif',
      'palatino' => '"Palatino Linotype", "URW Palladio L", "Book Antiqua", "Palatino", serif',
      'times' => '"Free Serif", "Times New Roman", Times, serif',
    );

    foreach ($fonts as $key => $value) {
      if ($font == $key) {
        $output = $value;
      }
    }
  }
  return $output;
}