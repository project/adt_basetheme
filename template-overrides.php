<?php
// Adds active/inactive to primary tabs
function phptemplate_menu_local_task($link, $active = FALSE) {
  return '<li '. ($active ? 'class="active" ' : 'class="inactive"') .'>'. $link ."</li>\n";
}

// Add span to tabs menu
function phptemplate_menu_item_link($link) {
  if (empty($link['localized_options'])) {
    $link['localized_options'] = array();
  }

  if ($link['tab_root']) {
  $options = array(
  $link['localized_options'],
  'HTML' => TRUE,
  );
  return l('<span class="tab">'.$link['title'].'</span>', $link['href'], array('html' => TRUE));
  } else {
  return l($link['title'], $link['href'], $link['localized_options']);
  }
}

// Add first/last/active classes to links
function phptemplate_links($links, $attributes = array('class' => 'links')) {
  $output = '';

  if (count($links) > 0) {
    $output = '<ul'. drupal_attributes($attributes) .'>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = $key;

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        $class .= ' first';
      }
      if ($i == $num_links) {
        $class .= ' last';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))) {
        $class .= ' active';
      } else {
        $class .= ' inactive';
      }
      $output .= '<li'. drupal_attributes(array('class' => $class)) .'>';

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      else if (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span'. $span_attributes .'>'. $link['title'] .'</span>';
      }
      $i++;
      $output .= "</li>\n";
    }

    $output .= '</ul>';
  }

  return $output;
}

/**
 * Split out taxonomy terms by vocabulary
 */
function phptemplate_print_terms($nid) {
  $vocabularies = taxonomy_get_vocabularies();
  $output = '<ul class="taxonomy">';
  foreach($vocabularies as $vocabulary) {
    if ($vocabularies) {
      $terms = taxonomy_node_get_terms_by_vocabulary($nid, $vocabulary->vid);
      if ($terms) {
        $links = array();
        $output .= '<li class="vocab ' . $vocabulary->name . '"><span class="vocab-name">' . $vocabulary->name . ': </span>';
      foreach ($terms as $term) {
        $links[] = l($term->name, taxonomy_term_path($term), array('rel' => 'tag', 'title' => strip_tags($term->description)));
      }
      $output .= implode(', ', $links);
      $output .= '</li>';
      }
    }
  }
   $output .= '</ul>';
   return $output;
}

// Add first/last classes to blocks
function phptemplate_blocks($region) {
  $output = '';

  if ($list = block_list($region)) {
    $i = 1;
    $count = count($list);
    foreach ($list as $key => $block) {
      // $key == <i>module</i>_<i>delta</i>
      if ($i == 1){ // is this the first block in this region?
        $block->first_last = 'first';
      }
      if ($i == $count){ // is this the last block in this region?
        $block->first_last = 'last';
      }
      $output .= theme('block', $block);
      $i++;
    }
  }

  // Add any content assigned to this region through drupal_set_content() calls.
  $output .= drupal_get_content($region);

  return $output;
}

// Override color module form to change color semantics and if necessary remove the preview
function phptemplate_color_scheme_form($form) {

  $form['palette']['base']['#title'] = 'Primary Color';
  $form['palette']['link']['#title'] = 'Secondary Color';
  $form['palette']['top']['#title'] = 'Tertiary Color';
  $form['palette']['bottom']['#title'] = 'Headings Color';

  // Include stylesheet
  $theme = $form['theme']['#value'];
  $info = $form['info']['#value'];
  drupal_add_css($theme_path . $info['preview_css']);

  // Wrapper
  $output .= '<div class="color-form clear-block">';

  // Color schemes
  $output .= drupal_render($form['scheme']);

  // Palette
  $output .= '<div id="palette" class="clear-block">';
  foreach (element_children($form['palette']) as $name) {
    $output .= drupal_render($form['palette'][$name]);
  }
  $output .= '</div>';

  // Preview
  $output .= drupal_render($form);
  $output .= '<div class="hide">'; // It's not possible to just remove the preview output, will cause javascript errors and will break form completely in ie
  $output .= '<h2>'. t('Preview') .'</h2>';
  $output .= '<div id="preview"><div id="text"><h2>Lorem ipsum dolor</h2><p>Sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud <a href="#">exercitation ullamco</a> laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></div><div id="img" style="background-image: url('. base_path() . $path . $info['preview_image'] .')" alt=""></div></div>';
  $output .= '</div>';

  // Close wrapper
  $output .= '</div>';

  return $output;
}