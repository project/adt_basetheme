Author: Jurriaan Roelofs
d.o. profile: http://drupal.org/user/52638
Website: http://www.clearwind.biz
Sponsored by: Alldrupalthemes.com

ABOUT THIS THEME

ADT Basetheme v2 is the basetheme for all new themes on alldrupalthemes.com and the themes produced by the Design to Drupal service on www.alldrupalthemes.com.

This theme includes a lot of code for theme settings and color module integration. If you do not wish to make use of theme settings and/or color module, you can easily strip the theme of these implementation by deleting the following files/functions:

FARBTASTIC COLOR WHEEL TIP

Use Google Chrome! Dragging the marker of the color picker is very resource intensive and Google Chrome uses the most efficient Javascript engine at the time of writing. In Firefox 3.0 or any version of Internet Explorer your color tuning experience is significantly more "Jitterish".

TABLE OF (file) CONTENTS

[color]                             Files for color module
[css]                               Stylesheets
[custom]                            Files for adding css and template.php things
[images]                            Layout images and icons
[preprocess]                        Adding and overriding variables for template files (preprocessing templates)
[scripts]                           Javascript files
[templates]                         Drupal template files
*.info                              Drupal Info file, defines regions and used stylesheets
template.php                        This file loads in all of the theme's php files
template-functions.php              Utility functions used by the theme
template-overrides.php              Functions overriding Drupal core or module functions
template-settings-controller.php    This file interprets theme settings and acts on them
theme-settings.php                  Definition of all the theme settings
theme-settings-defaults.php         Theme settings defaults