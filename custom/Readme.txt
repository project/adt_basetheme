These 2 files can be used to extend the Theme's stylesheet and template.php file with your own code. It is not recommended you change the main theme files because it will make it harder to upgrade your theme.

Usage:

1. Rename default.style.css to style.css and default.template.php to template.php
2. Add your code to these files as you would normally in the theme's main stylesheet and template.php files.