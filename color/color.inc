<?php
  //color schemes
  $info = array('schemes' => array(
    '#0077C0,#81CEFF,#00598E,#222222,#222222' => t('Druplicon Blue (Default)'),
    '#708500,#b4c945,#748708,#df8520,#222222' => t('Green and Orange'),
    '#000000,#808080,#000000,#000000,#222222' => t('Midnight'),
    '#afc000,#f0ff60,#828e00,#222222,#222222' => t('Lemon'),
  ),

  // Images to copy over
  'copy' => array(
    'logo.png',
    'images/bullet2.gif',
    'images/v0x-gradient-ultralight.gif',
    'images/v1.x-gradient-ultralight.gif',
    'images/x-gradient-ultralight.gif',
  ),

  // Color areas to fill (x, y, width, height)
  'fill' => array(
    'base' => array(0, 0, 2, 2),
  ),

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => array(
    'style.css',
  ),

  // Coordinates of all the theme slices (x, y, width, height)
  // with their filename as used in the stylesheet.
  'slices' => array(
    //'logo.png'               => array(0, 0, 2, 2),
  ),

  // color preview css
  'preview_css' => 'color/preview.css',

  // Base file for image generation
  'base_image' => 'color/base.png',

  //	blend target
	'blend_target' => '#ffffff'
);
?>