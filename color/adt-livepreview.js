$(document).ready(function(){
  
  var initPreview;
  var themePath = Drupal.settings.theme_path;
  var currentTheme = Drupal.settings.current_theme;
  var themeSettingsPage = Drupal.settings.t_s_page;

  // Update colors when farbtastic is updated
  $('.color-form input').click(function(event){
   updateStyles();
  });

  $('.color-form input').keyup(function(event){
   updateStyles();
  });

  $('.color-form select').click(function(event){
   updateStyles();
  });

  $('.color-form select').keyup(function(event){
   updateStyles();
  });
  
  $(document).bind('mousemove', checkDrag).bind('mouseup', checkDrag);

  function checkDrag() {
    if (document.dragging) {
      updateStyles();
    }
  }
  
  function updateStyles() {   
    
    if (!initPreview) {
      initPreview = true;
      // Set warning message
      if (themeSettingsPage != currentTheme) {
        $('#content .inner').prepend('<div class="messages error adt-livepreview-mssg">You are trying to set the colors for '+themeSettingsPage+' but the current theme is '+currentTheme+'. You can set the Default theme <a href="'+Drupal.settings.basePath+'admin/build/themes">here</a></div>');        
        $('fieldset#color_scheme_form').fadeTo('fast',0.2);
      } else {
        $('#content .inner').prepend('<div class="messages warning adt-livepreview-mssg">You are tuning colors in preview mode. Preview mode has some imperfections and will only give an approximate preview of the colorscheme. Submit the form to view the colorscheme in full effect.</div>');
      }
      
      // Replace normal images with PNG ones
      // $('#header img.logo').attr('src',''+themePath+'images/transparent/logo.png');
    }
    
    // From selectors as in the CSS theme-options file rendering in template.php... deleted all the selectors that are not needed on the colorform page to improve farbtastic performance.
    $('body').css('color',$('#edit-palette-text').val()); // dit overwrite alle css van alle tekst :|
    $('body a').css('color',$('#edit-palette-top').val());
    $('body, .poll .bar .foreground').css('background-color',$('#edit-palette-base').val());
    $('.adtblocks .block .block-inner').css('background-color',$('#edit-palette-link').val());
    $('h1, h2, h3, h4, h5, h6, legend, p.mission, p.slogan').css('color',$('#edit-palette-bottom').val());
    if(window.Cufon) {
    Cufon.refresh();
    }
  }

});