<?php
GLOBAL $theme;
$theme_path = drupal_get_path('theme', $theme) .'/';
$abs_theme_path .= base_path().$theme_path;

// Set container for ie6 targetted code
$vars['lte_ie6'] = '';
// Set container for ie7 targetted code
$vars['lte_ie7'] = '';

if(is_file($theme_path .'css/ie6fix.css')) {
  $vars['lte_ie6'] .= '<link rel="stylesheet" href="'. $abs_theme_path .'css/ie6fix.css" type="text/css">';
}
if(is_file($theme_path .'scripts/ie6/jQ.ie6hover.js')) {
  $vars['lte_ie6'] .= '<script type="text/javascript" src="'. $abs_theme_path .'scripts/ie6/jQ.ie6hover.js"></script>';
}
// Set container for ie7 targetted code
if(is_file($theme_path .'css/lte-ie7fix.css')) {
  $vars['lte_ie7'] .= '<link rel="stylesheet" href="'. $abs_theme_path .'css/lte-ie7fix.css" type="text/css">';
}

if (theme_get_setting('iepngfix') == 1) {
  if (is_file($theme_path .'scripts/ie6/iepngfix.htc')) {
  $vars['lte_ie6'] .= <<<EOD
  <style type="text/css">
  .content img, img.logo {
  behavior: url({$abs_theme_path}scripts/ie6/iepngfix.htc);
  }
  </style>
EOD;
  }
}

// Classes for body element. Allows advanced theming based on context
$body_classes = array();
$body_classes[] = ($vars['is_front']) ? 'front' : 'not-front';
$body_classes[] = ($vars['logged_in']) ? 'logged-in' : 'not-logged-in';
$close = ' sfy39587p00';
if ($vars['node']->type) {
  // If on an individual node page, put the node type in the body classes
  $body_classes[] = 'node-type-'. $vars['node']->type;
}
if ($vars['left'] && $vars['right']) {
  $body_classes[] = 'two-sidebars';
}
elseif ($vars['left']) {
  $body_classes[] = 'one-sidebar sidebar-left';
}
elseif ($vars['right']) {
  $body_classes[] = 'one-sidebar sidebar-right';
}
else {
  $body_classes[] = 'no-sidebars';
}
if (!$vars['is_front']) {
  // Add unique classes for each page and website section
  $path = drupal_get_path_alias($_GET['q']);
  list($section,) = explode('/', $path, 2);
  $body_classes[] = zen_id_safe('page-'. $path);
  // changes section to arg(0)
  $body_classes[] = zen_id_safe('section-'. arg(0));
  if (arg(0) == 'node') {
    if (arg(1) == 'add') {
      if ($section == 'node') {
        array_pop($body_classes); // Remove 'section-node'
      }
      $body_classes[] = 'section-node-add'; // Add 'section-node-add'
    }
    elseif (is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
      if ($section == 'node') {
        array_pop($body_classes); // Remove 'section-node'
      }
      $body_classes[] = 'section-node-'. arg(2); // Add 'section-node-edit' or 'section-node-delete'
    }
  }
}
$vars['closure'] .= '<div class="close">'. $close . '</div>';
$vars['body_classes'] = implode(' ', $body_classes); // Concatenate with spaces

// Generate Menus
$vars['primary_menu'] = menu_tree(variable_get('menu_primary_links_source', 'primary-links'));
$vars['secondary_menu'] = theme('links', $vars['secondary_links'], array('class' => 'links secondary-links'));

// Hook into color.module
if (module_exists('color')) {
_color_page_alter($vars);
}

// Widths of blocks in Dynamic regions
$precount = substr_count($vars['preblocks'], 'class="block ');
$postcount = substr_count($vars['postblocks'], 'class="block ');
$vars['postcount'] = $postcount;
$vars['precount'] = $precount;
/**
* If you would like to give these blocks a sidemargin of 1% the width would be "$prewidth = round(100/$precount,3)-2;" The minus 2 is the sum of side-margin, side-border width and side-padding you give to the div.block element.
* If you want a pixel or em based space between the blocks you should apply this to an inner block, never to the block itself as this can cause the sum of block widths to be more than 100% of the available space.
*/
if ($precount > 0) {
$prewidth = round(100/$precount,3);
}
if ($postcount > 0) {
$postwidth = round(100/$postcount,3);
}
$vars['blockwidths'] = <<<EOD
.preblocks .block {
width: $prewidth%;
}
.postblocks .block {
width:$postwidth%;
}
EOD;

// Sidebars And Middle column widths
$sidebarwidth = round(theme_get_setting('sidebar_width'),3);
if ((!$vars['left']) && (!$vars['right'])) {
$mainwidth = 100;
} elseif (($vars['left']) && ($vars['right'])) {
$mainwidth = round((100-$sidebarwidth*2),3);
$sidebarwidth = $sidebarwidth - 0.001; // Where 0.001 is the sum of side-margin, side-border width and side-padding on the sidebars plus 0.001.
} elseif (($vars['left']) OR ($vars['right'])) {
$mainwidth = round((100-$sidebarwidth),3);
$sidebarwidth = $sidebarwidth - 0.001;
}
$vars['structure_widths'] = <<<EOD
.sidebar {width: {$sidebarwidth}%;}
#content {width: {$mainwidth}%;}
EOD;
//end

// Link Icons
if (theme_get_setting('link_icons') == 1) {
$mylocation = $_SERVER['SERVER_NAME'];

$vars['linkicons'] .= <<<EOD
<link rel="stylesheet" href="{$abs_theme_path}css/linkicons.css" type="text/css" />
<style type="text/css" media="all">
a[href^='http://www.{$mylocation}'],
a[href^='http://{$mylocation}'] {
background:none;
padding-left:0px;
}
</style>
EOD;

$vars['lte_ie6'] .= <<<EOD
<link rel="stylesheet" href="{$abs_theme_path}css/linkicons-ielte6.css" type="text/css">
<script type="text/javascript" src="{$abs_theme_path}scripts/ie6/linkicons.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  $("a[href^='http://www.{$mylocation}']").removeClass("external");
  $("a[href^='http://{$mylocation}']").removeClass("external");
});
</script>
EOD;
}