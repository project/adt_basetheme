<?php
// Taxonomy
$taxonomy_content_type = (theme_get_setting('taxonomy_enable_content_type') == 1) ? $vars['node']->type : 'default';
$taxonomy_display = theme_get_setting('taxonomy_display_'. $taxonomy_content_type);
$taxonomy_format = theme_get_setting('taxonomy_format_'. $taxonomy_content_type);
if ((module_exists('taxonomy')) && ($taxonomy_display == 'all' || ($taxonomy_display == 'only' && $vars['page']))) {
  $vocabularies = taxonomy_get_vocabularies($vars['node']->type);
  $output = '';
  $vocab_delimiter = '';
  foreach ($vocabularies as $vocabulary) {
    if (theme_get_setting('taxonomy_vocab_display_'. $taxonomy_content_type .'_'. $vocabulary->vid) == FALSE) {
      $terms = taxonomy_node_get_terms_by_vocabulary($vars['node'], $vocabulary->vid);
      if ($terms) {
        $output .= ($taxonomy_format == 'vocab') ? '<li class="vocab vocab-'. $vocabulary->vid .'"><span class="vocab-name">'. $vocabulary->name .':</span> <ul class="vocab-list">' : '';
        $links = array();
        foreach ($terms as $term) {
          $links[] = '<li class="vocab-term">'. l($term->name, taxonomy_term_path($term), array('rel' => 'tag', 'title' => strip_tags($term->description))) .'</li>';
        }
        if ($taxonomy_format == 'list') {
          $output .= $vocab_delimiter;    // Add comma between vocabularies
          $vocab_delimiter = ', ';        // Use a comma delimiter after first displayed vocabulary
        }
        $output .= implode(", ", $links);
        $output .= ($taxonomy_format == 'vocab') ? '</ul></li>' : '';
      }
    }
  }
  if ($output != '') {
    $output = '<ul class="taxonomy">'. $output .'</ul>';
  }
  $vars['terms'] = $output;
}
else {
  $vars['terms'] = '';
}

// Node Links
if (isset($vars['node']->links['node_read_more'])) {
  $node_content_type = (theme_get_setting('readmore_enable_content_type') == 1) ? $vars['node']->type : 'default';
  $vars['node']->links['node_read_more'] = array(
    'title' => _themesettings_link(
    theme_get_setting('readmore_prefix_'. $node_content_type),
    theme_get_setting('readmore_suffix_'. $node_content_type),
    theme_get_setting('readmore_'. $node_content_type),
    'node/'. $vars['node']->nid,
    array(
      'attributes' => array('title' => theme_get_setting('readmore_title_'. $node_content_type)), 
      'query' => NULL, 'fragment' => NULL, 'absolute' => FALSE, 'html' => TRUE
      )
    ),
    'attributes' => array('class' => 'readmore-item'),
    'html' => TRUE,
  );
}
if (isset($vars['node']->links['comment_add'])) {
  $node_content_type = (theme_get_setting('comment_enable_content_type') == 1) ? $vars['node']->type : 'default';
  if ($vars['teaser']) {
    $vars['node']->links['comment_add'] = array(
      'title' => _themesettings_link(
      theme_get_setting('comment_add_prefix_'. $node_content_type),
      theme_get_setting('comment_add_suffix_'. $node_content_type),
      theme_get_setting('comment_add_'. $node_content_type),
      "comment/reply/".$vars['node']->nid,
      array(
        'attributes' => array('title' => theme_get_setting('comment_add_title_'. $node_content_type)), 
        'query' => NULL, 'fragment' => 'comment-form', 'absolute' => FALSE, 'html' => TRUE
        )
      ),
      'attributes' => array('class' => 'comment-add-item'),
      'html' => TRUE,
    );
  }
  else {
    $vars['node']->links['comment_add'] = array(
      'title' => _themesettings_link(
      theme_get_setting('comment_node_prefix_'. $node_content_type),
      theme_get_setting('comment_node_suffix_'. $node_content_type),
      theme_get_setting('comment_node_'. $node_content_type),
      "comment/reply/".$vars['node']->nid,
      array(
        'attributes' => array('title' => theme_get_setting('comment_node_title_'. $node_content_type)), 
        'query' => NULL, 'fragment' => 'comment-form', 'absolute' => FALSE, 'html' => TRUE
        )
      ),
      'attributes' => array('class' => 'comment-node-item'),
      'html' => TRUE,
    );
  }
}
if (isset($vars['node']->links['comment_new_comments'])) {
  $node_content_type = (theme_get_setting('comment_enable_content_type') == 1) ? $vars['node']->type : 'default';
  $vars['node']->links['comment_new_comments'] = array(
    'title' => _themesettings_link(
      theme_get_setting('comment_new_prefix_'. $node_content_type),
      theme_get_setting('comment_new_suffix_'. $node_content_type),
      format_plural(
        comment_num_new($vars['node']->nid),
        theme_get_setting('comment_new_singular_'. $node_content_type),
        theme_get_setting('comment_new_plural_'. $node_content_type)
      ),
      "node/".$vars['node']->nid,
      array(
        'attributes' => array('title' => theme_get_setting('comment_new_title_'. $node_content_type)), 
        'query' => NULL, 'fragment' => 'new', 'absolute' => FALSE, 'html' => TRUE
      )
    ),
    'attributes' => array('class' => 'comment-new-item'),
    'html' => TRUE,
  );
}
if (isset($vars['node']->links['comment_comments'])) {
  $node_content_type = (theme_get_setting('comment_enable_content_type') == 1) ? $vars['node']->type : 'default';
  $vars['node']->links['comment_comments'] = array(
    'title' => _themesettings_link(
      theme_get_setting('comment_prefix_'. $node_content_type),
      theme_get_setting('comment_suffix_'. $node_content_type),
      format_plural(
        comment_num_all($vars['node']->nid),
        theme_get_setting('comment_singular_'. $node_content_type),
        theme_get_setting('comment_plural_'. $node_content_type)
      ),
      "node/".$vars['node']->nid,
      array(
        'attributes' => array('title' => theme_get_setting('comment_title_'. $node_content_type)), 
        'query' => NULL, 'fragment' => 'comments', 'absolute' => FALSE, 'html' => TRUE
      )
    ),
    'attributes' => array('class' => 'comment-item'),
    'html' => TRUE,
  );
}
$vars['links'] = theme('links', $vars['node']->links, array('class' => 'links inline')); 

// Comments Header
if ((!$vars['teaser']) && theme_get_setting('commentheader') && ($vars['node']->comment_count > 0) && user_access('access comments')) {
  $vars['comment_header'] = '<h3 class="commentheader">'. theme_get_setting('commentheader') .'</h3>';
}

// Zen node classes
$node_classes = array();
if ($vars['sticky']) {
  $node_classes[] = 'sticky';
}
if (!$vars['node']->status) {
  $node_classes[] = 'node-unpublished';
  $vars['unpublished'] = TRUE;
}
else {
  $vars['unpublished'] = FALSE;
}
if ($vars['node']->uid && $vars['node']->uid == $user->uid) {
  // Node is authored by current user
  $node_classes[] = 'node-mine';
}
// Add a class to allow styling based on the node author.
$node_classes[] = 'author-' . zen_id_safe($vars['node']->name);
if ($vars['teaser']) {
  // Node is displayed as teaser
  $node_classes[] = 'node-teaser';
} else {
  $node_classes[] = 'node-full';
}
// Class for node type: "node-type-page", "node-type-story", "node-type-my-custom-type", etc.
$node_classes[] = 'node-type-'. $vars['node']->type;
// Class for node id
$node_classes[] = 'node-'. $vars['node']->nid;
$vars['node_classes'] = implode(' ', $node_classes); // Concatenate with spaces