  <div class="node <?php print $node_classes ?>">
    <?php if ($picture):
      print $picture;
    endif; ?>

    <?php if ($page == 0): ?>
    <h1 class="title nodetitle"><a href="<?php print $node_url?>"><?php print $title?></a></h1>
    <?php endif; ?>

    <?php if ($submitted): ?><span class="submitted"><?php print $submitted?></span><?php endif; ?>
    <div class="content"><?php print $content?></div>
    <?php if ($terms): ?>
    <div class="taxonomy">
      <?php print $terms; ?>
    </div>
    <?php endif;?>
    <?php if ($links): ?><div class="links"><?php print $links?></div><?php endif; ?>
  </div><!-- end .node -->

    <?php if ($comment_header):
      print $comment_header;
    endif; ?>