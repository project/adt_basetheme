  <div class="<?php print $block_classes; ?>" id="block-<?php print $block->module; ?>-<?php print $block->delta; ?>"><div class="block-inner">
	<?php if ($block->subject): ?>
		<h2 class="title blocktitle"><?php print $block->subject; ?></h2>
	<?php endif; ?>
    <div class="content">
      <?php print $block->content; ?>
      <?php if ($block->subject): ?>
        <?php print $edit_links; ?>
      <?php endif; ?>
    </div>
 </div></div><!-- end .inner --><!-- end .block -->
