<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>">
<head>
  <title><?php print $head_title ?></title>

  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>

  <style type="text/css">
  <?php
  /*** Widths of blocks in dynamic regions ***/
  print $blockwidths;
  /*** Sidebars And Middle column widths ***/
  print $structure_widths;
  ?>
  </style>

  <?php if ($lte_ie6): ?>
  <!--[if lte IE 6]>
      <?php print $lte_ie6 ?>
  <![endif]-->
  <?php endif; ?>

  <?php if ($lte_ie7): ?>
  <!--[if lte IE 7]>
      <?php print $lte_ie7 ?>
  <![endif]-->
  <?php endif; ?>

  <?php if ($linkicons): ?>
    <?php print $linkicons ?>
  <?php endif;?>

</head>

<body class="<?php print $body_classes ?>">

<p id="skipcontent"><a href="#content"><?php print t('Skip to content'); ?></a></p>

<div id="container" class="clearifx">

  <div id="header"><div class="inner clearfix">
    <?php if ($logo): ?>
      <a href="<?php print url() ?>" title="<?php t('Home') ?>"><img src="<?php print($logo) ?>" alt="Logo" class="logo"/></a>
    <?php endif; ?>

    <?php if ($site_name): ?>
    <h1 id="sitename"><a href="<?php print url() ?>" title="<?php t('Home') ?>"><?php print($site_name) ?></a></h1>
    <?php endif;?>

    <?php if ($site_slogan): ?>
    <p class="slogan"><?php print$site_slogan ?></p>
    <?php endif;?>

    <?php if ($secondary_links): ?>
      <?php print $secondary_menu ?>
    <?php endif; ?>
  </div></div><!-- end .inner --><!-- end #header -->

  <div id="main">

  <?php if ($primary_links): ?>
  <div id="navbar">
      <?php print $primary_menu ?>
  </div><!-- end #navbar -->
  <?php endif; ?>

  <?php if ($preblocks): ?>
  <div class="adtblocks clearfix preblocks precount<?php print $precount ?>">
  <?php print $preblocks ?>
  </div><!-- end .preblocks -->
  <?php endif; ?>

  <div class="wrapcolumns">
  <?php if ($left): ?>
  <div class="sidebar sleft"><div class="inner">
  <?php print $left ?>
  </div></div><!-- end .inner --><!-- end sidebar left -->
  <?php endif; ?>

  <div id="content"><div class="inner">
    <?php if ($breadcrumb) print $breadcrumb ?>

    <?php if ($tabs) print $tabs ?>

      <a name="content"></a>
      <?php if ($title): ?>
        <h1 class="content title" id="content-title"><?php print $title ?></h1>
      <?php endif; ?>

      <?php if ($mission): ?>
        <p class="mission"><?php print $mission ?></p>
      <?php endif; ?>

      <?php if ($help): ?>
        <p id="content-help"><?php print $help ?></p>
      <?php endif; ?>

      <?php if ($messages): ?>
        <div id="content-message"><?php print $messages ?></div>
      <?php endif; ?>

      <?php if ($content_top): ?>
        <div class="content-top"><?php print $content_top ?></div>
      <?php endif; ?>

      <?php print $content ?>

      <?php print $feed_icons; ?>

  </div></div><!-- end .inner --><!-- end #content -->

  <?php if ($right): ?>
  <div class="sidebar sright"><div class="inner">
  <?php print $right ?>
  </div></div><!-- end .inner --><!-- end sidebar right -->
  <?php endif; ?>
  </div><!-- end .wrapcolumns -->

  <?php if ($postblocks): ?>
  <div class="adtblocks clearfix postblocks postcount<?php print $postcount ?>">
  <?php print $postblocks ?>
  </div><!-- end .postblocks -->
  <?php endif; ?>

  </div><!-- end #main -->

  <div id="footer" class="clearfix">
    <?php if ($footer): ?>
      <?php print $footer ?>
    <?php endif; ?>
    <?php if ($footer_message): ?>
    <p id="footermessage"><?php print $footer_message ?></p>
    <?php // Since this is open source software you are allowed to remove the links below, but they are helpful to my website so if you could leave them, it's much appreciated! - peach ?>
    <p class="adt">Theme by <a href="http://www.alldrupalthemes.com" target="_blank">All Drupal Themes</a></p>
    <?php endif; ?>
  </div><!-- end #footer -->

</div><!-- end #container -->

<?php print $closure ?>

</body>
</html>