<div class="<?php print $comment_classes; ?>">

  <?php if ($title): ?>
    <h3 class="title commentheader"><?php print $title; if ($new): ?> <span class="new"><?php print $new; ?></span><?php endif; ?></h3>
  <?php endif; ?>

  <?php if ($unpublished): ?>
    <p class="unpublished"><?php print t('Unpublished'); ?></p>
  <?php endif; ?>

  <?php if ($picture) print $picture; ?>

  <p class="submitted">
    <?php print $submitted; ?>
  </p>

  <div class="content">
    <?php print $content; ?>
    <?php if ($signature): ?>
      <div class="signature">
        <?php print $signature ?>
      </div>
    <?php endif; ?>
  </div>

  <?php if ($links): ?>
    <div class="links">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

</div><!-- end .comment -->