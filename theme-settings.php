<?php

/**
* Implementation of THEMEHOOK_settings() function.
*
* @param $saved_settings
*   array An array of saved settings for this theme.
* @return
*   array A form array.
*/

function phptemplate_settings($saved_settings) {

  /* Store theme paths in variables - if you rename the theme you have to change the second parameter of the drupal_get_path function on the next line */
  $theme_path = drupal_get_path('theme', 'adt_basetheme') .'/';

  //Load Defaults
  if(is_file(drupal_get_path('theme', 'adt_basetheme') . '/theme-settings-defaults.php')) {
    include('theme-settings-defaults.php');
  }

  // Create the form widgets using Forms API
  $form['layout'] = array(
    '#title' => t('Layout Options'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
	);

	$form['layout']['fixedfluid'] = array(
    '#type' => 'radios',
    '#title' => t('Fixed or Fluid width'),
    '#default_value' => $settings['fixedfluid'],
    '#options' => array(
     'px' => t('Fixed width (in pixels)'),
     '%' => t('Fluid width (as percentage of browser width)')
    ),
  );

  $form['layout']['layout_width_fixed'] = array(
    '#type' => 'textfield',
    '#title' => t('FIXED Layout Width'),
    '#default_value' => $settings['layout_width_fixed'],
    '#size' => 4,
    '#maxlength' => 4,
    '#description' => t('Width in pixels, do not add trailing \'px\'. This field is only used when "Fixed" is selected above.'),
    '#element_validate' => array('is_number'),
  );

  $form['layout']['layout_width_fluid'] = array(
    '#type' => 'textfield',
    '#title' => t('FLUID Layout Width'),
    '#default_value' => $settings['layout_width_fluid'],
    '#size' => 3,
    '#maxlength' => 3,
    '#description' => t('Width in percentage of browser, do not add trailing \'%\'. This field is only used when "Fluid" is selected above.'),
    '#element_validate' => array('is_number'),
  );

  $form['layout']['sidebar_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Sidebar Width'),
    '#default_value' => $settings['sidebar_width'],
    '#size' => 6,
    '#maxlength' => 6,
    '#description' => t('Sidebar width as percentage of layout width. The main content area (center) automatically assumes the leftover width. If you want 3 equal width columns it\'s  best to fill in 33.333.'),
    '#element_validate' => array('is_number'),
  );

  $form['layout']['adv_layout'] = array(
    '#title' => t('Advanced Layout Options'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
	);

  $form['layout']['adv_layout']['layout_min_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Min-width'),
    '#default_value' => $settings['layout_min_width'],
    '#size' => 4,
    '#maxlength' => 4,
    '#description' => t('CSS min-width setting in pixels. Set 0 to disable.'),
    '#element_validate' => array('is_number'),
  );

  $form['layout']['adv_layout']['layout_max_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Max-width'),
    '#default_value' => $settings['layout_max_width'],
    '#size' => 4,
    '#maxlength' => 4,
    '#description' => t('CSS max-width setting in pixels. Set 0 to disable.'),
    '#element_validate' => array('is_number'),
  );

	$form['layout']['adv_layout']['expand_admin'] = array(
    '#type' => 'radios',
    '#title' => t('Expand admin pages'),
    '#default_value' => $settings['expand_admin'],
    '#options' => array(
     '0' => t('No'),
     '1' => t('Yes'),
    ),
     '#description' => t('Expand admin pages to 90% of browser width, to make your site more manageable if regular pages are narrow. Admin pages are all pages in the /admin path.')
  );

  // Typography Options
  $form['typography_options'] = array(
    '#title' => t('Typography options'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
	);

  // Regular Fonts Settings
  $form['typography_options']['body_font_face'] = array(
    '#type' => 'select',
    '#title' => t('Base Font'),
    '#default_value' =>  $settings['body_font_face'],
    '#options' => font_list(),
    '#description' => t('Select the font set for the body text.'),
  );

  $form['typography_options']['headings_font_face'] = array(
    '#type' => 'select',
    '#title' => t('Headings Font'),
    '#default_value' =>  $settings['headings_font_face'],
    '#options' => font_list(),
    '#description' => t('Select the font set for the headings.'),
  );

  // Cufon Font Replacement

  $form['typography_options']['cufon_font_replacement'] = array(
    '#title' => t('Cufon Font Replacement'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
	);

  $form['typography_options']['cufon_font_replacement']['cufon_enable'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Enable Cufon font replacement'),
    '#default_value' => $settings['cufon_enable'],
    '#prefix'        => '<p>Cufon is a script that replaces web fonts with an embedded font. This method does not have any effect on Search Engines or accessibility for screen-readers. You can generate you own Cufon fonts with the <a target=_blank" href="http://cufon.shoqolate.com/generate/">Cufon Generator</a>. For further instructions see (todo: write some documentation on this).</p>',
  );
  if(is_file($theme_path .'scripts/typography/cufon-yui.js')) {
    foreach (file_scan_directory($theme_path .'scripts/typography/fonts', '.js', array('.', '..', 'CVS')) as $file) {
      $cufon_files[$file->basename] = $file->name;
    }
    $cufon_font_face_suffix = '<p><strong>'. t('Available font example images:') .'</strong></p>';
    foreach (file_scan_directory($theme_path .'scripts/typography/fonts', '.png', array('.', '..', 'CVS')) as $file) {
      $cufon_font_face_suffix .= theme_image($theme_path .'scripts/typography/fonts/' .$file->basename);
    }
  } else {
  $cufon_font_face_suffix = '<p class="error">'. t('Cufon script not found, did you download the typography package as instructed on http://drupal.org/project/adt_basetheme?') .'</p>';
  }
  $form['typography_options']['cufon_font_replacement']['cufon_font_face'] = array(
    '#type'          => 'select',
    '#title'         => t('Cufon Font Face'),
    '#default_value' => $settings['cufon_font_face'],
    '#options'       => $cufon_files,
    '#suffix'        => $cufon_font_face_suffix,
  );


  // Node links and Taxonomy Links
  $form['node_options'] = array(
    '#title' => t('Node options'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
	);

  // Taxonomy Settings
  if (module_exists('taxonomy')) {
    $form['node_options']['display_taxonomy_container'] = array(
      '#type' => 'fieldset',
      '#title' => t('Taxonomy terms'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    // Default & content-type specific settings
    foreach ((array('default' => 'Default') + node_get_types('names')) as $type => $name) {
      // taxonomy display per node
      $form['node_options']['display_taxonomy_container']['display_taxonomy'][$type] = array(
        '#type' => 'fieldset',
        '#title'       => t('!name', array('!name' => t($name))),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
      // display
      $form['node_options']['display_taxonomy_container']['display_taxonomy'][$type]["taxonomy_display_{$type}"] = array(
        '#type'          => 'select',
        '#title'         => t('When should taxonomy terms be displayed?'),
        '#default_value' => $settings["taxonomy_display_{$type}"],
        '#options'       => array(
                              '' => '',
                              'never' => t('Never display taxonomy terms'),
                              'all' => t('Always display taxonomy terms'),
                              'only' => t('Only display taxonomy terms on full node pages'),
                            ),
      );
      // format
      $form['node_options']['display_taxonomy_container']['display_taxonomy'][$type]["taxonomy_format_{$type}"] = array(
        '#type'          => 'radios',
        '#title'         => t('Taxonomy display format'),
        '#default_value' => $settings["taxonomy_format_{$type}"],
        '#options'       => array(
                              'vocab' => t('Display each vocabulary on a new line'),
                              'list' => t('Display all taxonomy terms together in single list'),
                            ),
      );
      // Get taxonomy vocabularies by node type
      $vocabs = array();
      $vocabs_by_type = ($type == 'default') ? taxonomy_get_vocabularies() : taxonomy_get_vocabularies($type);
      foreach ($vocabs_by_type as $key => $value) {
        $vocabs[$value->vid] = $value->name;
      }
      // Display taxonomy checkboxes
      foreach ($vocabs as $key => $vocab_name) {
        $form['node_options']['display_taxonomy_container']['display_taxonomy'][$type]["taxonomy_vocab_display_{$type}_{$key}"] = array(
          '#type'          => 'checkbox',
          '#title'         => t('Hide vocabulary: '. $vocab_name),
          '#default_value' => $settings["taxonomy_vocab_display_{$type}_{$key}"],
        );
      }
      // Options for default settings
      if ($type == 'default') {
        $form['node_options']['display_taxonomy_container']['display_taxonomy']['default']['#title'] = t('Default');
        $form['node_options']['display_taxonomy_container']['display_taxonomy']['default']['#collapsed'] = $settings['taxonomy_enable_content_type'] ? TRUE : FALSE;
        $form['node_options']['display_taxonomy_container']['display_taxonomy']['taxonomy_enable_content_type'] = array(
          '#type'          => 'checkbox',
          '#title'         => t('Use custom settings for each content type instead of the default above'),
          '#default_value' => $settings['taxonomy_enable_content_type'],
        );
      }
      // Collapse content-type specific settings if default settings are being used
      else if ($settings['taxonomy_enable_content_type'] == 0) {
        $form['display_taxonomy'][$type]['#collapsed'] = TRUE;
      }
    }
  }

  // Read More & Comment Link Settings
  $form['node_options']['link_settings'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Links'),
    '#description' => t('Customize the text of node links'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
   );

  // Read more link settings
  $form['node_options']['link_settings']['readmore'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Read more'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
   );
  // Default & content-type specific settings
  foreach ((array('default' => 'Default') + node_get_types('names')) as $type => $name) {
    // Read more
    $form['node_options']['link_settings']['readmore'][$type] = array(
      '#type'        => 'fieldset',
      '#title'       => t('!name', array('!name' => t($name))),
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    );
    $form['node_options']['link_settings']['readmore'][$type]["readmore_{$type}"] = array(
      '#type'          => 'textfield',
      '#title'         => t('Link text'),
      '#default_value' => $settings["readmore_{$type}"],
      '#description'   => t('HTML is allowed.'),
    );
    $form['node_options']['link_settings']['readmore'][$type]["readmore_title_{$type}"] = array(
      '#type'          => 'textfield',
      '#title'         => t('Title text (tool tip)'),
      '#default_value' => $settings["readmore_title_{$type}"],
      '#description'   => t('Displayed when hovering over link. Plain text only.'),
    );
    $form['node_options']['link_settings']['readmore'][$type]["readmore_prefix_{$type}"] = array(
      '#type'          => 'textfield',
      '#title'         => t('Prefix'),
      '#default_value' => $settings["readmore_prefix_{$type}"],
      '#description'   => t('Text or HTML placed before the link.'),
    );
    $form['node_options']['link_settings']['readmore'][$type]["readmore_suffix_{$type}"] = array(
      '#type'          => 'textfield',
      '#title'         => t('Suffix'),
      '#default_value' => $settings["readmore_suffix_{$type}"],
      '#description'   => t('Text or HTML placed after the link.'),
    );
    // Options for default settings
    if ($type == 'default') {
      $form['node_options']['link_settings']['readmore']['default']['#title'] = t('Default');
      $form['node_options']['link_settings']['readmore']['default']['#collapsed'] = $settings['readmore_enable_content_type'] ? TRUE : FALSE;
      $form['node_options']['link_settings']['readmore']['readmore_enable_content_type'] = array(
        '#type'          => 'checkbox',
        '#title'         => t('Use custom settings for each content type instead of the default above'),
        '#default_value' => $settings['readmore_enable_content_type'],
      );
    }
    // Collapse content-type specific settings if default settings are being used
    else if ($settings['readmore_enable_content_type'] == 0) {
      $form['readmore'][$type]['#collapsed'] = TRUE;
    }
  }

  // Comments link settings
  $form['node_options']['link_settings']['comment'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Comment'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  );
  // Default & content-type specific settings
  foreach ((array('default' => 'Default') + node_get_types('names')) as $type => $name) {
    $form['node_options']['link_settings']['comment'][$type] = array(
      '#type'        => 'fieldset',
      '#title'       => t('!name', array('!name' => t($name))),
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    );
    $form['node_options']['link_settings']['comment'][$type]['add'] = array(
      '#type'        => 'fieldset',
      '#title'       => t('Add new comment link'),
      '#description' => t('The link when there are no comments.'),
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    );
    $form['node_options']['link_settings']['comment'][$type]['add']["comment_add_{$type}"] = array(
      '#type'          => 'textfield',
      '#title'         => t('Link text'),
      '#default_value' => $settings["comment_add_{$type}"],
      '#description'   => t('HTML is allowed.'),
    );
    $form['node_options']['link_settings']['comment'][$type]['add']["comment_add_title_{$type}"] = array(
      '#type'          => 'textfield',
      '#title'         => t('Title text (tool tip)'),
      '#default_value' => $settings["comment_add_title_{$type}"],
      '#description'   => t('Displayed when hovering over link. Plain text only.'),
    );
    $form['node_options']['link_settings']['comment'][$type]['add']['extra'] = array(
      '#type'        => 'fieldset',
      '#title'       => t('Advanced'),
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    );
    $form['node_options']['link_settings']['comment'][$type]['add']['extra']["comment_add_prefix_{$type}"] = array(
      '#type'          => 'textfield',
      '#title'         => t('Prefix'),
      '#default_value' => $settings["comment_add_prefix_{$type}"],
      '#description'   => t('Text or HTML placed before the link.'),
    );
    $form['node_options']['link_settings']['comment'][$type]['add']['extra']["comment_add_suffix_{$type}"] = array(
      '#type'          => 'textfield',
      '#title'         => t('Suffix'),
      '#default_value' => $settings["comment_add_suffix_{$type}"],
      '#description'   => t('Text or HTML placed after the link.'),
    );
    $form['node_options']['link_settings']['comment'][$type]['standard'] = array(
      '#type'        => 'fieldset',
      '#title'       => t('Comments link'),
      '#description' => t('The link when there are one or more comments.'),
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    );
    $form['node_options']['link_settings']['comment'][$type]['standard']["comment_singular_{$type}"] = array(
      '#type'          => 'textfield',
      '#title'         => t('Link text when there is 1 comment'),
      '#default_value' => $settings["comment_singular_{$type}"],
      '#description'   => t('HTML is allowed.'),
    );
    $form['node_options']['link_settings']['comment'][$type]['standard']["comment_plural_{$type}"] = array(
      '#type'          => 'textfield',
      '#title'         => t('Link text when there are multiple comments'),
      '#default_value' => $settings["comment_plural_{$type}"],
      '#description'   => t('HTML is allowed. @count will be replaced with the number of comments.'),
    );
    $form['node_options']['link_settings']['comment'][$type]['standard']["comment_title_{$type}"] = array(
      '#type'          => 'textfield',
      '#title'         => t('Title text (tool tip)'),
      '#default_value' => $settings["comment_title_{$type}"],
      '#description'   => t('Displayed when hovering over link. Plain text only.'),
    );
    $form['node_options']['link_settings']['comment'][$type]['standard']['extra'] = array(
      '#type'        => 'fieldset',
      '#title'       => t('Advanced'),
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    );
    $form['node_options']['link_settings']['comment'][$type]['standard']['extra']["comment_prefix_{$type}"] = array(
      '#type'          => 'textfield',
      '#title'         => t('Prefix'),
      '#default_value' => $settings["comment_prefix_{$type}"],
      '#description'   => t('Text or HTML placed before the link.'),
    );
    $form['node_options']['link_settings']['comment'][$type]['standard']['extra']["comment_suffix_{$type}"] = array(
      '#type'          => 'textfield',
      '#title'         => t('Suffix'),
      '#default_value' => $settings["comment_suffix_{$type}"],
      '#description'   => t('Text or HTML placed after the link.'),
    );
    $form['node_options']['link_settings']['comment'][$type]['new'] = array(
      '#type'        => 'fieldset',
      '#title'       => t('New comments link'),
      '#description' => t('The link when there are one or more new comments.'),
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    );
    $form['node_options']['link_settings']['comment'][$type]['new']["comment_new_singular_{$type}"] = array(
      '#type'          => 'textfield',
      '#title'         => t('Link text when there is 1 new comment'),
      '#default_value' => $settings["comment_new_singular_{$type}"],
      '#description'   => t('HTML is allowed.'),
    );
    $form['node_options']['link_settings']['comment'][$type]['new']["comment_new_plural_{$type}"] = array(
      '#type'          => 'textfield',
      '#title'         => t('Link text when there are multiple new comments'),
      '#default_value' => $settings["comment_new_plural_{$type}"],
      '#description'   => t('HTML is allowed. @count will be replaced with the number of comments.'),
    );
    $form['node_options']['link_settings']['comment'][$type]['new']["comment_new_title_{$type}"] = array(
      '#type'          => 'textfield',
      '#title'         => t('Title text (tool tip)'),
      '#default_value' => $settings["comment_new_title_{$type}"],
      '#description'   => t('Displayed when hovering over link. Plain text only.'),
    );
    $form['node_options']['link_settings']['comment'][$type]['new']['extra'] = array(
      '#type'        => 'fieldset',
      '#title'       => t('Advanced'),
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    );
    $form['node_options']['link_settings']['comment'][$type]['new']['extra']["comment_new_prefix_{$type}"] = array(
      '#type'          => 'textfield',
      '#title'         => t('Prefix'),
      '#default_value' => $settings["comment_new_prefix_{$type}"],
      '#description'   => t('Text or HTML placed before the link.'),
    );
    $form['node_options']['link_settings']['comment'][$type]['new']['extra']["comment_new_suffix_{$type}"] = array(
      '#type'          => 'textfield',
      '#title'         => t('Suffix'),
      '#default_value' => $settings["comment_new_suffix_{$type}"],
      '#description'   => t('Text or HTML placed after the link.'),
    );
    // Options for default settings
    if ($type == 'default') {
      $form['node_options']['link_settings']['comment']['default']['#title'] = t('Default');
      $form['node_options']['link_settings']['comment']['default']['#collapsed'] = $settings['comment_enable_content_type'] ? TRUE : FALSE;
      $form['node_options']['link_settings']['comment']['comment_enable_content_type'] = array(
        '#type'          => 'checkbox',
        '#title'         => t('Use custom settings for each content type instead of the default above'),
        '#default_value' => $settings['comment_enable_content_type'],
      );
    }
    // Collapse content-type specific settings if default settings are being used
    else if ($settings['comment_enable_content_type'] == 0) {
      $form['comment'][$type]['#collapsed'] = TRUE;
    }
  }

	$form['node_options']['commentheader'] = array(
    '#type' => 'textfield',
    '#title' => t('Comments Header'),
    '#default_value' => $settings['commentheader'],
    '#description' => t('Displays a header above your comments, leave blank to disable.'),
  );

  $form['dropdown'] = array(
    '#title' => t('Dropdown Menu Options'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
	);

  // Width of Drop-down menus
  $form['dropdown']['sublevel_width'] = array(
    '#type' => 'select',
    '#title' => t('Drop-down Menus Sub-Level Menu Width'),
    '#default_value' => $settings['sky_sub_navigation_size'],
    '#options' => adt_size_range(7, 30, 'em', 10),
    '#prefix' => '<p>'. t('Note: Drop-down menus go up to 4 levels deep.') .'</p>',
    '#description' => t('Sets the width of all the Sublevels of the dropdown menu.'),
  );

  $form['dropdown']['superfish'] = array(
    '#title' => t('Drop-down Animation (Superfish)'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
	);

  $form['dropdown']['superfish']['superfish_enable'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Enable Superfish drop-down animation'),
    '#default_value' => $settings['superfish_enable'],
  );

  $form['dropdown']['superfish']['superfish_speed'] = array(
    '#type' => 'textfield',
    '#title' => t('Animation Speed'),
    '#default_value' => $settings['superfish_speed'],
    '#size' => 6,
    '#maxlength' => 6,
    '#description' => t('Animation speed in milliseconds. Do not add "ms" to the number.'),
    '#element_validate' => array('is_number'),
  );

  $form['dropdown']['superfish']['superfish_delay'] = array(
    '#type' => 'textfield',
    '#title' => t('Mouse-Out Delay'),
    '#default_value' => $settings['superfish_delay'],
    '#size' => 6,
    '#maxlength' => 6,
    '#description' => t('Time in milliseconds the sublevel menus remain visible when you mouse out of the menu. A study by usability expert Jacob Nielsen recommends 500 milliseconds. Do not add "ms" to the number. Set 0 to disable.'),
    '#element_validate' => array('is_number'),
  );

	$form['dropdown']['superfish']['superfish_easing'] = array(
    '#type' => 'radios',
    '#title' => t('Animation Easing'),
    '#default_value' => $settings['superfish_easing'],
    '#options' => array(
     'linear' => t('Linear'),
     'swing' => t('Swing (Default)'),
    ),
    '#description' => t('This determines how the animation gains speed. Swing looks especially nice on larger sub-menes'),
  );

  $form['dropdown']['superfish']['superfish_properties'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Animation set-up'),
    '#default_value' => $settings['superfish_properties'],
    '#options' => array(
      'opacity' => t('Fade in'),
      'width' => t('Fold out sideways'),
      'height' => t('Fold out downwards'),
    ),
    '#description' => t('Select the properties you would like to animate. You can select as many properties as you like.'),
  );

  $form['misc'] = array(
    '#title' => t('Misc Options'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
	);

	$form['misc']['link_icons'] = array(
    '#type' => 'radios',
    '#title' => t('Link Icons'),
    '#default_value' => $settings['link_icons'],
    '#prefix'        => '<p>Icons can be <a href="http://www.alldrupalthemes.com/sites/www.alldrupalthemes.com/files/basetheme/icons.zip">downloaded here</a> and extracted to images/icons in the theme folder.</p>',
    '#options' => array(
     '0' => t('No'),
     '1' => t('Yes'),
    ),
    '#description' => t('Standards compliant browsers with load icons through CSS3 selectors (no javascript!). Older browsers (internet explorer 6 and below) will load the icons through a tiny jquery javascript.'),
  );

  $form['misc']['block_edit'] = array(
    '#type' => 'radios',
    '#title' => t('Edit-Block links'),
    '#default_value' => $settings['block_edit'],
    '#options' => array(
     '0' => t('No'),
     '1' => t('Yes'),
    ),
    '#description' => t('Add edit links on all blocks to allow easy access to blocks management.'),
  );

	$form['misc']['force_eq_heights'] = array(
    '#type' => 'radios',
    '#title' => t('Enforce Equal heights'),
    '#default_value' => $settings['force_eq_heights'],
    '#options' => array(
     '0' => t('No'),
     '1' => t('Yes'),
    ),
    '#description' => t('Enforce equal heights on horizontally aligned blocks (it looks better).'),
  );

	$form['misc']['input_example'] = array(
    '#type' => 'radios',
    '#title' => t('Preload Example Text'),
    '#default_value' => $settings['input_example'],
    '#options' => array(
     '0' => t('No'),
     '1' => t('Yes'),
    ),
    '#description' => t('Loads example text in compact forms (with no labels).'),
  );

  $form['misc']['iepngfix'] = array(
    '#type' => 'radios',
    '#title' => t('Use IE PNG Fix'),
    '#default_value' => $settings['iepngfix'],
    '#options' => array(
     '0' => t('No'),
     '1' => t('Yes'),
    ),
    '#description' => t('Fix Alpha-transparency in PNG support for internet explorer 6.'),
  );

  $form['timestamp'] = array(
    '#type' => 'value',
    '#value' => time(),
  );

/**
 * Validation Function to enforce numeric values
 */
function is_number($formelement, &$form_state) {
  $thevalue = $formelement['#value'];
  $title = $formelement['#title'];
  if (!is_numeric($thevalue)) {
    form_error($formelement, t("<em>$title</em> must be a number."));
  }
}
  // Return the additional form widgets
  return $form;
}

/**
 * Helper function to provide a list of fonts for select list in theme settings.
 * Originally by Jacine @ Sky theme - thx ;)
 */
function font_list() {
  $fonts = array(
    'Sans-serif' => array(
      'arial' => t('Arial, Helvetica'),
      'verdana' => t('Verdana'),
      'lucida' => t('Lucida Grande, Lucida Sans Unicode'),
      'geneva' => t('Geneva'),
      'tahoma' => t('Tahoma'),
      'century' => t('Century Gothic'),
    ),
    'Serif' => array(
      'georgia' => t('Georgia'),
      'palatino' => t('Palatino Linotype, Book Antiqua'),
      'times' => t('Times New Roman'),
    ),
  );
  return $fonts;
}

/**
 * Helper function to provide a list of sizes for use in theme settings.
 * Originally by Jacine @ Sky theme - thx ;)
 */
function adt_size_range($start = 11, $end = 16, $unit = 'px', $default = NULL) {
  $range = '';
  if (is_numeric($start) && is_numeric($end)) {
    $range = array();
    $size = $start;
    while ($size >= $start && $size <= $end) {
      if ($size == $default) {
        $range[$size . $unit] = $size . $unit .' (default)';
      }
      else {
        $range[$size . $unit] = $size . $unit;
      }
      $size++;
    }
  }
  return $range;
}