This is where the miscellaneous scripts go, currently there are 2 jQuery plugins here:

jQuery.equalHeights: This plugin selects a set of elements and then sets them all to be the same height as the tallest element.

jquery.example.min: This plugin can be used to preload text into a text form field.