The superfish script is the same as the original from http://users.tpg.com.au/j_birch/plugins/superfish/ other than a hack to support more easing options. The original script had hardcoded linear easing into the animate() function.

The invoke-superfis.js file controls which menu the script targets. By default it is the ul tag inside the #navbar div, so if you change the location of the menu in the DOM you have to modify the invoke-superfish.js script.
