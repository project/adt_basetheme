		// initialise plugins
  $(document).ready(function() {
    $('#navbar > ul').superfish({
      hoverClass:  'over',           // hover class
      pathClass:   false,            // active class on li
      delay:     Drupal.settings.superfish.delay,                // 500ms delay on mouseout as per Jacob Nielsen advice
      //animation:   {opacity:'show',height:'show',width:'show'}, // fade-in and slide-down animation
      animation:   Drupal.settings.superfish.animation, // Load animation object
      speed:     Drupal.settings.superfish.speed+'ms',            // animation speed
      easing:    Drupal.settings.superfish.easing,            // easing linear/swing
      autoArrows:  false,              // generation of arrow mark-up
      dropShadows: true               // drop shadows
    });
  });